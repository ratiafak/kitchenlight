/*
 * pwm.h
 *
 *  Created on: Jun 2, 2018
 *      Author: Tomas Jakubik
 *
 * PWM controlling two mosfets driving two LED strips.
 */

#ifndef PWM_H_
#define PWM_H_

#include "stm32l0xx.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Start PWM.
 */
void pwm_init();

/**
 * @brief Set PWM value
 * @param out0 duty cycle of output 0 [0 ~ 0xffff]
 * @param out1 duty cycle of output 1 [0 ~ 0xffff]
 */
void pwm_set(uint32_t out0, uint32_t out1);

#ifdef __cplusplus
}
#endif

#endif /* PWM_H_ */
