/*
 * hw.h
 *
 *  Created on: Jun 2, 2018
 *      Author: Tomas Jakubik
 */

#ifndef HW_H_
#define HW_H_

#include "stm32l0xx.h"

#ifdef __cplusplus
extern "C" {
#endif

///Green LED
#define HW_LED_G_INIT()  do{RCC->IOPENR |= RCC_IOPENR_IOPBEN; GPIOB->MODER = (GPIOB->MODER & ~(0x3 << (3*2))) | (0x1 << (3*2));}while(0)
#define HW_LED_G_ON()    do{GPIOB->BSRR = (1 << 3);}while(0)
#define HW_LED_G_OFF()   do{GPIOB->BSRR = (1 << (3+16));}while(0)

///Inputs
#define HW_IN_0_INIT()   do{RCC->IOPENR |= RCC_IOPENR_IOPAEN; GPIOA->MODER &= ~(0x3 << (7*2));}while(0)
#define HW_IN_0_STATE()  (!(GPIOA->IDR & (1 << 7)))
#define HW_IN_1_INIT()   do{RCC->IOPENR |= RCC_IOPENR_IOPAEN; GPIOA->MODER &= ~(0x3 << (6*2));}while(0)
#define HW_IN_1_STATE()  (!(GPIOA->IDR & (1 << 6)))
#define HW_IN_STATE()    (HW_IN_0_STATE() || HW_IN_1_STATE())

///Time
extern volatile uint32_t hw_time;	///< Time variable incremented in systick
#define HW_TIME_1ms      (1)
#define HW_TIME_1s       (HW_TIME_1ms*1000)
#define HW_TIME_1min     (HW_TIME_1s*60)
void hw_init_clock();

#ifdef __cplusplus
}
#endif

#endif /* HW_H_ */
