/*
 * pwm.c
 *
 *  Created on: Jun 2, 2018
 *      Author: Tomas Jakubik
 *
 * PWM controlling two mosfets driving two LED strips.
 */

#include "pwm.h"

/**
 * @brief Start PWM.
 */
void pwm_init()
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM22EN;	//Enable clock for timer

	TIM22->CCER = 0;	//Normal polarity, outputs off
	TIM22->PSC = 3;	//Divide clock by
	TIM22->ARR = 0xffe;	//12 bits minus one
	TIM22->CCMR1 = (0x6 << 4) | (1 << 3) | (1 << 2)	//PWM1, preload active
			| (0x6 << 12) | (1 << 11) | (1 << 10);	//PWM1, preload active
	TIM22->SMCR = 0;	//No slave function
	TIM22->CR2 = 0;	//No master function
	TIM22->CCR1 = 0;	//Start off
	TIM22->CCR2 = 0;	//Start off
	TIM22->EGR = 0x1;	//Update and load values

	RCC->IOPENR |= RCC_IOPENR_IOPAEN;	//Enable clock for GPIO
	GPIOA->AFR[1] = (GPIOA->AFR[1] & ~0xff0) | 0x550;	//Select AF5 = TIM22
	GPIOA->MODER = (GPIOA->MODER & ~((0x3 << (10*2)) | (0x3 << (9*2)))) | (0x2 << (10*2)) | (0x2 << (9*2));	//Set alternate function
	GPIOA->OSPEEDR |= (0x3 << (10*2)) | (0x3 << (9*2));

	TIM22->CR1 = 0x1;	//Enable timer
	TIM22->CCER |= (1 << 0) | (1 << 4);	//Enable outputs
}

/**
 * @brief Set PWM value
 * @param out0 12 bit duty cycle of output 0 [0 ~ 0xfff]
 * @param out1 12 bit duty cycle of output 1 [0 ~ 0xfff]
 */
void pwm_set(uint32_t out0, uint32_t out1)
{
	if(out0 > 0xfff) out0 = 0xfff;
	if(out1 > 0xfff) out1 = 0xfff;
	TIM22->CCR1 = out0;	//Preload output compare, it is used at next overflow
	TIM22->CCR2 = out1;
}

