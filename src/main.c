/*
 * main.c
 *
 *  Created on: Jun 2, 2018
 *      Author: Tomas Jakubik
 *
 * Main file of kitchen light controller project.
 */

#include <string.h>
#include "stm32l0xx.h"
#include "hw.h"
#include "pwm.h"

#define DUTY_N     256
static const uint32_t dutytable[DUTY_N] =
{
	0, 1, 2, 3, 5, 7, 9, 11,
	13, 16, 19, 21, 24, 28, 31, 34,
	38, 41, 45, 49, 53, 57, 61, 65,
	70, 74, 79, 84, 88, 93, 98, 103,
	108, 114, 119, 124, 130, 135, 141, 147,
	153, 159, 165, 171, 177, 183, 190, 196,
	203, 209, 216, 223, 230, 237, 244, 251,
	258, 266, 273, 281, 288, 296, 304, 311,
	319, 327, 335, 344, 352, 360, 369, 377,
	386, 394, 403, 412, 421, 430, 439, 449,
	458, 467, 477, 486, 496, 506, 516, 526,
	536, 546, 556, 566, 577, 587, 598, 608,
	619, 630, 641, 652, 663, 675, 686, 697,
	709, 721, 732, 744, 756, 768, 781, 793,
	805, 818, 830, 843, 856, 869, 882, 895,
	908, 921, 935, 948, 962, 976, 990, 1004,
	1018, 1032, 1046, 1061, 1075, 1090, 1105, 1120,
	1135, 1150, 1165, 1181, 1196, 1212, 1228, 1244,
	1260, 1276, 1292, 1309, 1325, 1342, 1359, 1376,
	1393, 1410, 1427, 1445, 1462, 1480, 1498, 1516,
	1534, 1553, 1571, 1590, 1608, 1627, 1646, 1666,
	1685, 1704, 1724, 1744, 1764, 1784, 1804, 1825,
	1845, 1866, 1887, 1908, 1929, 1951, 1972, 1994,
	2016, 2038, 2060, 2083, 2105, 2128, 2151, 2174,
	2197, 2221, 2244, 2268, 2292, 2316, 2341, 2365,
	2390, 2415, 2440, 2466, 2491, 2517, 2543, 2569,
	2595, 2622, 2649, 2676, 2703, 2730, 2758, 2786,
	2814, 2842, 2870, 2899, 2928, 2957, 2986, 3016,
	3046, 3076, 3106, 3137, 3167, 3198, 3230, 3261,
	3293, 3325, 3357, 3389, 3422, 3455, 3488, 3522,
	3556, 3590, 3624, 3659, 3693, 3728, 3764, 3799,
	3835, 3872, 3908, 3945, 3982, 4019, 4057, 4095,
};



#define FILTER_TOP            32	///< Filter counter top
#define FILTER_ON             27	///< Filter counter set value
#define FILTER_OFF             5	///< Filter counter reset value
#define TIME_DIM             500	///< Delay before dimming starts
#define TIME_DIM_STEP         20	///< Dimming happens once in this many ticks

typedef struct
{
	uint32_t cnt;	///< Counter of the filter
	uint32_t state;	///< State of the input after filter
	uint32_t time_on;	///< Counts time the input is active
	uint32_t latch;	///< Set when rising edge turns lights on to prevent falling edge from turning them off
	uint32_t dim_step;	///< Delay between individual dim steps
} input_filter_t;	///< Structure of variables for one input_check()
uint32_t input_check(input_filter_t* internal, uint32_t in, uint32_t duty_set);	///< Check one input and set lights

/**
 * @brief Main function of kitchen light controller.
 */
int main(void)
{
	uint32_t duty_out = 0;	///< Proportional light intensity, current value
	uint32_t duty_set = 0;	///< Proportional light intensity, setpoint value

	input_filter_t internal0;	///< Internals for checking input 0
	memset(&internal0, 0, sizeof(input_filter_t));	///< Init structure
	input_filter_t internal1;	///< Internals for checking input 0
	memset(&internal1, 0, sizeof(input_filter_t));	///< Init structure

	hw_init_clock();	//Init clock
	HW_LED_G_INIT();	//Init green LED
	HW_IN_0_INIT();	//Init input 0
	HW_IN_1_INIT();	//Init input 1
	pwm_init();	//Init PWM output

	while(1)
	{
		duty_set = input_check(&internal0, HW_IN_0_STATE(), duty_set);	//Check input 0
		duty_set = input_check(&internal1, HW_IN_1_STATE(), duty_set);	//Check input 1

		//Dimming
		if((duty_set > duty_out) && (duty_out < (DUTY_N-1)))
		{
			duty_out++;	//Increase light
		}
		else if((duty_set < duty_out) && (duty_out > 0))
		{
			duty_out--;	//Decrease light
		}

		pwm_set(dutytable[duty_out], dutytable[duty_out]);	//Set PWM

		__WFI();	//Sleep until next systick
	}

	return 0;
}


/**
 * @brief Check one input, filter it, and set lights.
 *   Call once every tick for each input.
 * @param internal internal state
 * @param in input value
 * @param duty_set input light intensity
 * @return output new duty_set
 */
uint32_t input_check(input_filter_t* internal, uint32_t in, uint32_t duty_set)
{
	if(in)	//High input
	{
		if(internal->cnt < FILTER_TOP) internal->cnt++;	//Increment counter
	}
	else	//Low input
	{
		if(internal->cnt > 0) internal->cnt--;	//Decrement counter
	}

	if(internal->cnt > FILTER_ON)	//Filter counter over threshold
	{
		if(!internal->state)	//Rising edge
		{
			internal->time_on = 0;	//Reset timer
			if(duty_set == 0)
			{
				duty_set = DUTY_N-1;	//Lights on
				internal->latch = 1;	//Latch to block falling edge
			}
			else
			{
				internal->latch = 0;	//Reset latch
			}
		}
		else
		{
			internal->time_on++;	//Count time the input is on
			if((internal->time_on > TIME_DIM) && (duty_set > 0))	//Input is active for long and lights are on
			{
				if(internal->dim_step > TIME_DIM_STEP)
				{
					duty_set--;	//Decrease light slowly
					internal->dim_step = 0;
				}
				else
				{
					internal->dim_step++;
				}
			}
		}
		internal->state = 1;	//Set
	}
	else if(internal->cnt < FILTER_OFF) 	//Filter counter under threshold
	{
		if(internal->state)	//Falling edge
		{
			if((internal->time_on < TIME_DIM) && !internal->latch)	//Short action, didn't start the lights on this action
			{
				duty_set = 0;	//Lights off
			}
		}
		internal->state = 0;	//Reset
	}
	return duty_set;
}






