/*
 * hw.c
 *
 *  Created on: Jun 2, 2018
 *      Author: Tomas Jakubik
 */

#include "hw.h"

volatile uint32_t hw_time = 0;	///< Time variable incremented in systick

/**
 * @brief Initialize PLL for system clock and start systick.
 */
void hw_init_clock()
{
	if(!(RCC->CR & RCC_CR_HSIRDY))	//Test if HSI16 is on
	{
		RCC->CR |= RCC_CR_HSION;	//Enable HSI16
		while(!(RCC->CR & RCC_CR_HSIRDY))	//Wait for HSI16 to be ready
		{
			__NOP();	//Get stuck here
		}
	}

	if((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL)	//Test if PLL is used as System clock
	{
		RCC->CFGR = (RCC->CFGR & (uint32_t)(~RCC_CFGR_SW)) | RCC_CFGR_SW_HSI;	//Select HSI as system clock
		while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI)	//Wait for HSI switched
		{
			__NOP();	//Get stuck here
		}
	}

	RCC->CR &= (uint32_t)(~RCC_CR_PLLON);	//Disable the PLL
	while((RCC->CR & RCC_CR_PLLRDY) != 0)	//Wait until PLLRDY is cleared
	{
		__NOP();	//Get stuck here
	}

	FLASH->ACR |= FLASH_ACR_LATENCY;	//Set latency to 1 wait state
	//Set the PLL multiplier and divider
	RCC->CFGR = (RCC->CFGR & (~(RCC_CFGR_PLLMUL | RCC_CFGR_PLLDIV | RCC_CFGR_PLLSRC))) | (RCC_CFGR_PLLMUL4 | RCC_CFGR_PLLDIV2);

	RCC->CR |= RCC_CR_PLLON;	//Enable the PLL
	while((RCC->CR & RCC_CR_PLLRDY) == 0)	//Wait until PLLRDY is set
	{
		__NOP();	//Get stuck here
	}
	RCC->CFGR |= (uint32_t)(RCC_CFGR_SW_PLL);	//Select PLL as system clock
	while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL)	//Wait until the PLL is switched on
	{
		__NOP();	//Get stuck here
	}

	SystemCoreClockUpdate();	//Update SystemCoreClock

	SysTick_Config(SystemCoreClock/HW_TIME_1s-1);	//Start sytick
}

/**
 * @brief Systick called periodically.
 */
void SysTick_Handler()
{
	hw_time++;	//Increment time
}

